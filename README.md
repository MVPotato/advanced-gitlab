# Advanced GitLab stuff - Dynamically create stages, jobs and environments

When starting the pipeline, only 2 stages with 1 job each are known.

![](images/1.png)

But as soon as we run the trigger job, a downstream pipeline appears.

![](images/2.png)

The great thing is, we can now create dynamical jobs and therefore also environments etc.

![](images/3.png)
